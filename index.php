<?php
$conexion = new mysqli("localhost", "root", "", "liga");
if ($conexion->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $conexion->connect_errno . ") " . $conexion->connect_error;
}else{
  $resultado = $conexion->query("SELECT * FROM partido");
}
 ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  </head>
  <body>
    <div class="container">
      <nav>
        <div class="nav-wrapper">
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="index.php">INICIO</a></li>
          </ul>
        </div>
      </nav>
      <h2>Partidos</h2>
      <table>
        <tr>
          <th>id</th>
          <th>Local</th>
          <th>Visitante</th>
          <th>Resultado</th>
        </tr>
        <?php
          foreach ($resultado as $equipo) {
            echo "<tr>";
            echo "<td>".$equipo['id_partido']."</td>";
            echo "<td>"."<a href=equipo.php?equipo=".$equipo['local'].">".$equipo['local']."</a> </td>";
            echo "<td>"."<a href=equipo.php?equipo=".$equipo['visitante'].">".$equipo['visitante']."</a> </td>";
            echo "<td>".$equipo['resultado']."</td>";
            echo "</tr>";
          }
        ?>
      </table>

      <h2>INSERTAR PARTIDO</h2>

      <form method="POST" action="insertarpartido.php">
        Equipo Local:<br>
        <input type="text" name="local" pattern=[0-6]><br>
        Equipo Visitante:<br>
        <input type="text" name="visitante" pattern=[0-6]><br>
        Resultado:
        <input type="text" name="resultado"><br><br>
        <input type="submit" value="Submit">
      </form>

    </div>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </body>
</html>

