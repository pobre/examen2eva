<?php
$conexion = new mysqli("localhost", "root", "", "liga");
if ($conexion->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $conexion->connect_errno . ") " . $conexion->connect_error;
}else{
    $jugadoresentrada = $_GET['jugadores'];
  $resultado = $conexion->query("SELECT * FROM jugador WHERE equipo = $jugadoresentrada");
}
 ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  </head>
  <body>
    <div class="container">
      <nav>
        <div class="nav-wrapper">
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="index.php">PARTIDOS</a></li>
          </ul>
        </div>
      </nav>
      <h2>Jugadores</h2>
      <table>
        <tr>
          <th>id</th>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Posición</th>
          <th>Salario</th>
          <th>Altura</th>
        </tr>
        <?php
          foreach ($resultado as $jugadorx) {
              if ($jugadorx['posicion'] == "Base") {
                  echo "<tr>";
                  echo "<td>"."<b>".$jugadorx['id_jugador']."</b>"."</td>";
                  echo "<td>"."<b>".$jugadorx['nombre']."</b>"."</td>";
                  echo "<td>"."<b>".$jugadorx['apellido']."</b>"."</td>";
                  echo "<td>"."<b>".$jugadorx['posicion']."</b>"."</td>";
                  echo "<td>"."<b>".$jugadorx['salario']."</b>"."</td>";
                  echo "<td>"."<b>".$jugadorx['altura']."</b>"."</td>";
                  echo "</tr>";
              }else{
                echo "<tr>";
                echo "<td>".$jugadorx['id_jugador']."</td>";
                echo "<td>".$jugadorx['nombre']."</td>";
                echo "<td>".$jugadorx['apellido']."</td>";
                echo "<td>".$jugadorx['posicion']."</td>";
                echo "<td>".$jugadorx['salario']."</td>";
                echo "<td>".$jugadorx['altura']."</td>";
                echo "</tr>";
              }
          }
        ?>
      </table>
    </div>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </body>
</html>